"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fastify_1 = __importDefault(require("fastify"));
const authRoute_1 = __importDefault(require("./routes/authRoute"));
const errorHandler_1 = __importDefault(require("./middlewares/errorHandler"));
// Server config
const server = (0, fastify_1.default)();
server.setErrorHandler(errorHandler_1.default);
// Routes
server.register(authRoute_1.default);
exports.default = server;
