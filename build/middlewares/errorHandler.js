"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// Error Handler
const errorHandler = (error, request, reply) => {
    const statusCode = reply.statusCode == 200 ? 500 : reply.statusCode;
    reply.status(statusCode);
    reply.send(error);
};
exports.default = errorHandler;
