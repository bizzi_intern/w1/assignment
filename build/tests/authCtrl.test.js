"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt_1 = __importDefault(require("bcrypt"));
const app_1 = __importDefault(require("../app"));
const connectDb_1 = __importDefault(require("../config/connectDb"));
const saltRounds = 10;
const testUser = {
    username: "__test_user__",
    password: "1",
};
const existUser = {
    username: "__exist_user__",
    password: "1",
};
// Create exist user and remove test user for register testing
beforeAll(async () => {
    const dbUsers = connectDb_1.default.get("users").value();
    const findUser = dbUsers.find((dbUser) => dbUser.username === existUser.username);
    // Create exist user
    if (!findUser) {
        const salt = await bcrypt_1.default.genSaltSync(saltRounds);
        const hashedPw = await bcrypt_1.default.hash(existUser.password, salt);
        const hashedUser = {
            username: existUser.username,
            password: hashedPw,
        };
        connectDb_1.default.get("users").push(hashedUser).write();
    }
    // Remove test user
    connectDb_1.default.get("users").remove({ username: testUser.username }).write();
});
// Remove test and exist user
afterAll(() => {
    connectDb_1.default.get("users").remove({ username: testUser.username }).write();
    connectDb_1.default.get("users").remove({ username: existUser.username }).write();
});
describe("user register", () => {
    test("should create new user if user doesn't exist", async () => {
        const reply = await app_1.default.inject({
            method: "POST",
            url: "/api/user/register",
            payload: testUser,
        });
        const dbUsers = connectDb_1.default.get("users").value();
        const findUser = dbUsers.find((dbUser) => (dbUser.username = testUser.username));
        expect(findUser?.username).toBe(testUser.username);
        expect(reply.statusCode).toBe(200);
    });
    test("should receive 409 if user exists", async () => {
        const reply = await app_1.default.inject({
            method: "POST",
            url: "/api/user/register",
            payload: existUser,
        });
        expect(reply.statusCode).toBe(409);
    });
});
describe("user login", () => {
    test("should receive 200 and ok message if login successfully", async () => {
        const reply = await app_1.default.inject({
            method: "POST",
            url: "/api/user/login",
            payload: testUser,
        });
        expect(reply.statusCode).toBe(200);
        expect(JSON.parse(reply.payload)).toHaveProperty("msg");
    });
    test("should receive 401 if user enters wrong username or not registered", async () => {
        const userInput = {
            username: "testuser",
            password: "1",
        };
        const reply = await app_1.default.inject({
            method: "POST",
            url: "/api/user/login",
            payload: userInput,
        });
        expect(reply.statusCode).toBe(401);
    });
    test("should receive 401 if user enters wrong password", async () => {
        const userInput = {
            username: "__test_user__",
            password: "123",
        };
        const reply = await app_1.default.inject({
            method: "POST",
            url: "/api/user/login",
            payload: userInput,
        });
        expect(reply.statusCode).toBe(401);
    });
});
