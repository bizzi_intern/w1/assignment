"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.registerUser = exports.loginUser = void 0;
const bcrypt_1 = __importDefault(require("bcrypt"));
const connectDb_1 = __importDefault(require("../config/connectDb"));
const saltRounds = 10;
const registerUser = async (request, reply) => {
    const { username, password } = request.body;
    const salt = await bcrypt_1.default.genSaltSync(saltRounds);
    const hashedPw = await bcrypt_1.default.hash(password, salt);
    await connectDb_1.default.read();
    const users = connectDb_1.default.get("users").value();
    const findUser = users.find((user) => user.username === username);
    if (!findUser) {
        // Create a new User
        connectDb_1.default.get("users")
            .push({
            username: username,
            password: hashedPw,
        })
            .write();
        reply.send({
            msg: "Register ok",
        });
    }
    else {
        reply.status(409);
        throw new Error("User Already Exists");
    }
};
exports.registerUser = registerUser;
const loginUser = async (request, reply) => {
    const { username, password } = request?.body;
    const users = connectDb_1.default.get("users").value();
    const findUser = users.find((user) => user.username === username);
    if (!findUser) {
        reply.status(401);
        throw new Error("Wrong Username / User Not Registered Error");
    }
    if (!(await bcrypt_1.default.compare(password, findUser?.password))) {
        reply.status(401);
        throw new Error("Wrong Password Error");
    }
    reply.send({ msg: "Login ok" });
};
exports.loginUser = loginUser;
