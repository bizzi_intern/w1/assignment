"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const authCtrl_1 = require("../controllers/authCtrl");
const authRoute = (app, _, done) => {
    app.post("/api/user/login", authCtrl_1.loginUser);
    app.post("/api/user/register", authCtrl_1.registerUser);
    done();
};
exports.default = authRoute;
