"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lowdb_1 = __importDefault(require("lowdb"));
const FileSync_1 = __importDefault(require("lowdb/adapters/FileSync"));
const connectDb = () => {
    const adapter = new FileSync_1.default("src/database/db.json");
    const db = (0, lowdb_1.default)(adapter);
    db.defaults({
        users: [],
    });
    return db;
};
const db = connectDb();
exports.default = db;
