import "dotenv/config";
import app from "./app";

const myPort = Number(process.env.PORT) || 5050;

app.listen({ port: myPort }, async (err, address) => {
  if (err) {
    console.error(err);
    process.exit(1);
  }
  console.log(`Server is listening at ${address}`);
});

export default app;
