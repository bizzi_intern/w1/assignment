import { FastifyReply, FastifyRequest } from "fastify";

// Error Handler
const errorHandler = (
  error: Error,
  request: FastifyRequest,
  reply: FastifyReply
) => {
  const statusCode = reply.statusCode == 200 ? 500 : reply.statusCode;
  reply.status(statusCode);
  reply.send(error);
};

export default errorHandler;
