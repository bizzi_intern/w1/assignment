import fastify from "fastify";
import authRoute from "./routes/authRoute";
import errorHandler from "./middlewares/errorHandler";

// App config
const app = fastify();
app.setErrorHandler(errorHandler);

// Routes
app.register(authRoute);

export default app;
