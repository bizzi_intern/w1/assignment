import { FastifyReply, FastifyRequest } from "fastify";
import bcrypt from "bcrypt";
import User from "../interfaces/UserInterface";
import db from "../config/connectDb";

const saltRounds = 10;

const registerUser = async (
  request: FastifyRequest<{ Body: User }>,
  reply: FastifyReply
) => {
  const { username, password } = request.body;
  const salt = await bcrypt.genSaltSync(saltRounds);
  const hashedPw = await bcrypt.hash(password, salt);

  await db.read();
  const users = db.get("users").value();
  const findUser = users.find((user: User) => user.username === username);

  if (!findUser) {
    // Create a new User
    (<any>db.get("users"))
      .push({
        username: username,
        password: hashedPw,
      })
      .write();

    reply.send({
      msg: "Register ok",
    });
  } else {
    reply.status(409);
    throw new Error("User Already Exists");
  }
};

const loginUser = async (request: any, reply: any) => {
  const { username, password } = request?.body;
  const users = db.get("users").value();
  const findUser = users.find((user: User) => user.username === username);

  if (!findUser) {
    reply.status(401);
    throw new Error("Wrong Username / User Not Registered Error");
  }
  if (!(await bcrypt.compare(password, findUser?.password))) {
    reply.status(401);
    throw new Error("Wrong Password Error");
  }

  reply.send({ msg: "Login ok" });
};

export { loginUser, registerUser };
