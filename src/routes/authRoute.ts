import { FastifyInstance } from "fastify";
import { loginUser, registerUser } from "../controllers/authCtrl";

const authRoute = (app: FastifyInstance, _: Object, done: Function) => {
  app.post("/api/user/login", loginUser);
  app.post("/api/user/register", registerUser);
  done();
};

export default authRoute;
